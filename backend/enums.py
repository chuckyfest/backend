from enum import Enum, unique


@unique
class DjangoChoiceEnum(Enum):
    @classmethod
    def to_choices(cls):
        return map(lambda c: (c.name, c.value), cls)

    @classmethod
    def list_names(cls):
        return map(lambda c: c.name, cls)

    @classmethod
    def list_values(cls):
        return map(lambda c: c.value, cls)

    @classmethod
    def get_max_length(cls):
        return len(max(cls.list_names(), key=len))


class SleepingPlaces(DjangoChoiceEnum):
    tent = "Tente"
    bedroom = "Chambre"
    plus_one = "Plus un"


class TransportModes(DjangoChoiceEnum):
    train = "Train"
    car = "Voiture"


class FromCity(DjangoChoiceEnum):
    nantes = "Nantes"
    paris = "Paris"
    vlp = "VLP"
    tours = "Tours"


class Seasons(DjangoChoiceEnum):
    summer = "Été"
    winter = "Hiver"
