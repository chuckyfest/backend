"""
WSGI config for backend project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""

import os
import sys

import django
from django.conf import settings
from django.core.handlers.wsgi import WSGIHandler

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from backend.config import get_settings

if not settings.configured:
    settings.configure(**get_settings())
    django.setup(set_prefix=False)

application = WSGIHandler()
