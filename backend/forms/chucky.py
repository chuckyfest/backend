from django import forms

from backend.models import UserEdition


class ChuckyForm(forms.ModelForm):
    start_datetime = forms.SplitDateTimeField(required=False)
    end_datetime = forms.SplitDateTimeField(required=False)

    class Meta:
        model = UserEdition
        fields = (
            "start_datetime",
            "end_datetime",
            "ask_bedroom",
            "want_to_canoe",
            "transport_message",
            "accept_date_sharing",
        )
