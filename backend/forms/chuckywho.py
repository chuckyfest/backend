from django import forms

from backend.models import UserProfile


class ChuckyWhoForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = (
            "phone",
            "special_diet",
            # "curriculum_vitae",
            # "cover_letter",
            "accept_email_sharing",
            "accept_phone_sharing",
            "immor_ale",
            "feod_ale",
            "glaci_ale",
            "spectr_ale",
            "brut_ale",
            "infern_ale",
            "ancestr_ale",
            "abyss_ale",
        )
