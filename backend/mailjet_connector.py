"""
This call sends a message to the given recipient with vars and custom vars.
"""
from django.conf import settings
from mailjet_rest import Client

api_key = settings.MJ_HOST_USER
api_secret = settings.MJ_HOST_PASSWORD
mailjet = Client(auth=(api_key, api_secret), version="v3.1")


def send_mail(subject, template_id, recipient_list, variables):
    data = {
        "Messages": [
            {
                "From": {"Email": "chucky@chuckyfest.fr", "Name": "Chucky"},
                "To": recipient_list,
                "TemplateID": template_id,
                "TemplateLanguage": True,
                "Subject": subject,
                "Variables": variables,
            }
        ]
    }
    # print(result.status_code)
    # print(result.json())
    return mailjet.send.create(data=data)


# to test :
# from django.conf import settings
# from backend.mailjet_connector import send_mail; send_mail("Yooo", settings.MJ_MAIL_LOGIN_ID, [{"Email": "mechin.emmanuel@lilo.org", "Name": "Emmanuel Méchin"}], dict(token="abc"))
