const container = document.getElementById('beerDistribution');
console.log(unknownDistribution)
new Chart(container, {
    type: 'bar',
    data: {
        labels: ['Inconnu', '0€', '10€', '20€', '30€', '40€', '50€', '60€'],
        datasets: [
            {
                label: 'Payé (nb de pers)',
                data: paidDistribution,
                borderWidth: 1,
                backgroundColor: '#a5dfdf',
            },
            {
                label: 'Annoncé (nb de pers)',
                data: announcedDistribution,
                borderWidth: 1,
                backgroundColor: '#ffe6aa',
            },
            {
                label: 'Inconnu (nb de pers)',
                data: unknownDistribution,
                borderWidth: 1,
                backgroundColor: '#e4e5e7',
            }
        ]
    },
    options: {
        scales: {
            x: {
                stacked: true,
            },
            y: {
                beginAtZero: true,
                stacked: true
            }
        }
    }
});
