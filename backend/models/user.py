from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.db.models import fields, ObjectDoesNotExist, Model
from django.db.models.fields.related import ManyToManyField, OneToOneField, CASCADE
from django.contrib import admin
from django.http.response import HttpResponse

from rest_framework.authtoken.models import Token

from backend.mailjet_connector import send_mail
from .edition import Edition
from .user_edition import UserEdition


class UserProfile(Model):
    user = OneToOneField(User, on_delete=CASCADE, related_name="user_profile")

    welcome_mail_sent = fields.BooleanField("Mail d'accueil envoyé", default=False)

    phone = fields.CharField("Téléphone", max_length=18, blank=True)
    special_diet = fields.CharField("Régime alimentaire", max_length=1000, blank=True)
    # curriculum_vitae = fields.FileField()
    # cover_letter = fields.FileField()
    chucky_editions = ManyToManyField(
        Edition,
        verbose_name="Éditions Chucky",
        blank=True,
        related_name="persons",
        through="UserEdition",
        through_fields=("user", "edition"),
    )
    accept_email_sharing = fields.BooleanField(
        "Accepte le partage de son email", default=False
    )
    accept_phone_sharing = fields.BooleanField(
        "Accepte le partage de son téléphone", default=False
    )
    immor_ale = fields.IntegerField(
        verbose_name="Vote pour L'Immor'Ale", null=True, blank=True
    )
    feod_ale = fields.IntegerField(
        verbose_name="Vote pour La Féod'Ale", null=True, blank=True
    )
    glaci_ale = fields.IntegerField(
        verbose_name="Vote pour La Glaci'Ale", null=True, blank=True
    )
    spectr_ale = fields.IntegerField(
        verbose_name="Vote pour La Spectr'Ale", null=True, blank=True
    )
    brut_ale = fields.IntegerField(
        verbose_name="Vote pour La Brut'Ale", null=True, blank=True
    )
    infern_ale = fields.IntegerField(
        verbose_name="Vote pour La Infern'Ale", null=True, blank=True
    )
    ancestr_ale = fields.IntegerField(
        verbose_name="Vote pour La Ancestr'Ale", null=True, blank=True
    )
    abyss_ale = fields.IntegerField(
        verbose_name="Vote pour La Abyss'Ale", null=True, blank=True
    )

    class Meta:
        app_label = "backend"
        ordering = ["user__first_name", "user__last_name"]

    def __str__(self):
        display_name = self.get_display_name()
        if display_name is None:
            return "{email}".format(email=self.email)
        else:
            return "{display_name}".format(display_name=display_name)

    def get_display_name(self):
        if (
            self.user.first_name is not None
            and self.user.first_name != ""
            or self.user.last_name is not None
            and self.user.last_name != ""
        ):
            if self.user.first_name is None or self.user.first_name == "":
                return self.user.last_name
            elif self.user.last_name is None or self.user.last_name == "":
                return self.user.first_name
            else:
                return "{first_name} {last_name}".format(
                    first_name=self.user.first_name, last_name=self.user.last_name
                )
        else:
            return self.user.email

    def get_user_edition(self, edition):
        try:
            return UserEdition.objects.get(user=self, edition=edition)
        except ObjectDoesNotExist:
            return UserEdition.objects.create(user=self, edition=edition)

    def get_current_user_edition(self, current_edition=None):
        """current_edition can be given to avoid requests if already requested"""
        if not current_edition:
            current_edition = Edition.objects.get(is_current=True)

        return self.get_user_edition(current_edition)

    @classmethod
    def get_content_type(cls):
        from django.contrib.contenttypes.models import ContentType

        return ContentType.objects.get(app_label="backend", model="user-profile")

    # def get_token(self, renew=True):
    #     from rest_framework.authtoken.models import Token
    #
    #     try:
    #         token = Token.objects.get(user=self)
    #     except Token.DoesNotExist:
    #         pass
    #     else:
    #         if not renew:
    #             return token.key
    #         else:
    #             token.delete()
    #     return Token.objects.create(user=self).key


@admin.action(description="Reset mail envoyé")
def reset_for_new_edition(modeladmin, request, queryset):
    for user_profile in queryset:
        user_profile.welcome_mail_sent = False
        user_profile.save()


@admin.action(description="Récupérer mails")
def fetch_mail(modeladmin, request, queryset):
    url = request.META.get("HTTP_REFERER")
    emails = ";".join([ue.user.email for ue in queryset])
    return HttpResponse(
        f"""
        <div style="font-size: 3em; text-align: center">
            <input value="{emails}" style="width: 100%; font-size: 1em; " />
            <a href="{url}" style="display: block; margin: 100px; text-decoration: none; background: grey; padding: 30px; color: white">Retour</a>
            <script>document.getElementsByTagName('input')[0].select()</script>
        </div>
        """
    )


@admin.action(description="Créer les user_editions pour chaque user")
def create_user_edition_for_current_edition(modeladmin, request, queryset):
    current_edition = Edition.objects.get(is_current=True)
    for user_profile in queryset:
        # this is actually a get_or_create, so it automatically creates the link instance
        user_profile.get_current_user_edition(current_edition)


class EditionInline(admin.TabularInline):
    from .user_edition import UserEdition

    model = UserEdition
    fk_name = "user"


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ("__str__", "welcome_mail_sent", "special_diet")
    list_filter = ("welcome_mail_sent", "chucky_editions")
    inlines = [EditionInline]
    actions = [
        reset_for_new_edition,
        fetch_mail,
        create_user_edition_for_current_edition,
    ]
    search_fields = (
        "user__first_name",
        "user__last_name",
        "user__username",
    )


admin.site.register(UserProfile, UserProfileAdmin)


@admin.action(description="Envoyer mail d'accueil")
def send_welcome_mail(modeladmin, request, queryset):
    for user in queryset:
        try:
            token = user.auth_token
        except ObjectDoesNotExist:
            token = Token.objects.create(user=user)

        current_edition = Edition.objects.get(is_current=True)

        if not settings.DEBUG:
            res = send_mail(
                f"Chucky Festival {current_edition} !",
                settings.MJ_WELCOME_ID,
                [dict(Email=user.email, Name=user.first_name)],
                dict(token=token.key),
            )
            if res.status_code == 200:
                print("mail sent")
        else:
            print(f"Le token : f{token.key}")

        try:
            user.user_profile.welcome_mail_sent = True
            user.user_profile.save()
        except ObjectDoesNotExist:
            UserProfile.objects.create(user=user, welcome_mail_sent=True)


class UserAdmin(BaseUserAdmin):
    @admin.display(description="Mail d'accueil envoyé", boolean=True)
    def welcome_mail_sent(self, user):
        return user.user_profile.welcome_mail_sent

    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "username", "password1", "password2"),
            },
        ),
    )
    list_display = [d for d in BaseUserAdmin.list_display] + ["welcome_mail_sent"]
    list_filter = [f for f in BaseUserAdmin.list_filter] + [
        "user_profile__welcome_mail_sent"
    ]
    actions = BaseUserAdmin.actions + [send_welcome_mail]


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
