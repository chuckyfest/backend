from django.db.models import (
    Model,
    ForeignKey,
    CASCADE,
    IntegerField,
    CharField,
    FloatField,
    DecimalField,
)
from django.contrib import admin


class Expense(Model):
    budget = ForeignKey("Budget", on_delete=CASCADE)
    name = CharField("Nom", max_length=100)
    amount = DecimalField("Montant", max_digits=7, decimal_places=2)

    @property
    def edition(self):
        return self.budget.edition

    class Meta:
        app_label = "backend"

    def __str__(self):
        return f"Dépense {self.name} du {str(self.budget)}"

    @classmethod
    def get_content_type(cls):
        from django.contrib.contenttypes.models import ContentType

        return ContentType.objects.get(app_label="backend", model="expense")


class ExpenseAdmin(admin.ModelAdmin):
    list_display = ("__str__", "edition", "budget", "name", "amount")
    fields = ("budget", "name", "amount")
    readonly_fields = ()
    ordering = ("-budget", "name")
    list_filter = ("budget", "budget__edition", "name")


admin.site.register(Expense, ExpenseAdmin)
