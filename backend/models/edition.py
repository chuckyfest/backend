from statistics import mean

from django.db.transaction import atomic
from django.db.models import Model, fields
from django.contrib import admin

from backend.enums import Seasons

from .expense import Expense


class Edition(Model):
    year = fields.IntegerField("Année")
    season = fields.CharField(
        "Saison",
        max_length=Seasons.get_max_length(),
        default=Seasons.summer.name,
        choices=Seasons.to_choices(),
    )
    is_current = fields.BooleanField(verbose_name="Édition en cours", default=False)
    start_date = fields.DateField(verbose_name="Date de début", null=True)
    end_date = fields.DateField(verbose_name="Date de fin", null=True)
    payable = fields.BooleanField(
        verbose_name="Édition payable en avance", default=True
    )
    signal_group_id = fields.CharField(
        verbose_name="ID du groupe Signal", null=True, blank=True, max_length=72
    )
    signal_transport_group_id = fields.CharField(
        verbose_name="ID du groupe Signal pour le transport",
        null=True,
        blank=True,
        max_length=72,
    )
    photo_album_link = fields.CharField(
        verbose_name="Lien de l'album photo", null=True, blank=True, max_length=100
    )
    payment_iban = fields.CharField(
        verbose_name="IBAN du compte pour le paiement", max_length=33
    )
    payment_bic = fields.CharField(
        verbose_name="BIC du compte pour le paiement", max_length=11
    )
    payment_owner_name = fields.CharField(
        verbose_name="Nom du titulaire du compte pour le paiement", max_length=20
    )
    lydia_link = fields.URLField(
        verbose_name="Lien Cagnotte Lydia", null=True, blank=True, max_length=80
    )

    @property
    def coming_persons(self):
        return self.useredition_set.filter(is_coming=True)

    @property
    def coming_count(self):
        return len(
            list(
                filter(
                    lambda user_edition: user_edition.contribution_amount,
                    self.useredition_set.all(),
                )
            )
        )

    @property
    def total_budget(self):
        return sum(
            map(
                lambda user_edition: user_edition.contribution_amount,
                self.useredition_set.all(),
            )
        )

    @property
    def total_bank(self):
        return sum(self.useredition_set.all().values_list("paid_amount", flat=True))

    @property
    def budget_due(self):
        return sum(
            map(
                lambda user_edition: user_edition.due_amount, self.useredition_set.all()
            )
        )

    @property
    def beer_budget(self):
        return sum(
            map(
                lambda user_edition: user_edition.beer_amount
                + user_edition.beer_amount_announced,
                self.useredition_set.all(),
            )
        )

    @property
    def total_budgeted(self):
        return sum(map(lambda budget: budget.amount(), self.budget_set.all()))

    @property
    def not_budgeted(self):
        return (
            self.total_budget
            + self.closed_editions_total_not_spent
            + self.beer_budget
            - self.total_budgeted
        )

    @property
    def closed_editions_total_not_spent(self):
        if not self.is_current:
            return 0

        # because sometimes the next edition is created early to push cotisation in the future.
        # so we need here to exclude this editions
        previous_closed_editions = Edition.objects.filter(
            is_current=False, year__lt=self.year
        )

        return sum(
            map(
                lambda edition: edition.not_budgeted + edition.not_spent,
                previous_closed_editions,
            )
        )

    @property
    def total_expenses(self):
        return sum(
            map(
                lambda expense: expense.amount,
                Expense.objects.filter(budget__edition=self),
            )
        )

    @property
    def not_spent(self):
        return self.total_budgeted - self.total_expenses

    def _get_ale_votes(self, ale_vote_attr_name):
        coming_people = self.useredition_set.select_related("user").filter(
            is_coming=True
        )
        return list(
            filter(
                lambda vote: vote[1] is not None,
                map(
                    lambda user_edition: (
                        user_edition.user.get_display_name(),
                        getattr(user_edition.user, ale_vote_attr_name),
                    ),
                    coming_people,
                ),
            )
        )

    def _get_ale_mean_vote(self, ale_vote_attr_name):
        votes = list(map(lambda vote: vote[1], self._get_ale_votes(ale_vote_attr_name)))
        if not votes:
            return None
        return round(mean(votes), 1)

    def _get_ale_mean_votes_str(self, ale_vote_attr_name):
        votes = self._get_ale_votes(ale_vote_attr_name)
        mean_votes = round(mean(map(lambda vote: vote[1], votes)), 1)

        str_votes = ", ".join(map(lambda vote: f"{vote[0]}: {vote[1]}", votes))
        return f"MOYENNE: {mean_votes} - - - - - - - {str_votes}"

    @property
    def immor_ale_mean_votes(self):
        return self._get_ale_mean_vote("immor_ale")

    @property
    def feod_ale_mean_votes(self):
        return self._get_ale_mean_vote("feod_ale")

    @property
    def glaci_ale_mean_votes(self):
        return self._get_ale_mean_vote("glaci_ale")

    @property
    def spectr_ale_mean_votes(self):
        return self._get_ale_mean_vote("spectr_ale")

    @property
    def brut_ale_mean_votes(self):
        return self._get_ale_mean_vote("brut_ale")

    @property
    def infern_ale_mean_votes(self):
        return self._get_ale_mean_vote("infern_ale")

    @property
    def ancestr_ale_mean_votes(self):
        return self._get_ale_mean_vote("ancestr_ale")

    @property
    def abyss_ale_mean_votes(self):
        return self._get_ale_mean_vote("abyss_ale")

    class Meta:
        app_label = "backend"

    def __str__(self):
        return f"Édition {self.year} - {self.get_season_display()}"

    @classmethod
    def get_content_type(cls):
        from django.contrib.contenttypes.models import ContentType

        return ContentType.objects.get(app_label="backend", model="edition")

    def save(self, *args, **kwargs):
        if not self.is_current:
            return super().save(*args, **kwargs)
        with atomic():
            self.__class__.objects.filter(is_current=True).update(is_current=False)
            return super().save(*args, **kwargs)

    def get_event_invitation_file_name(self):
        return f"{self.start_date.year}_{self.season}.ics"


class PresentInline(admin.TabularInline):
    from .user_edition import UserEdition

    model = UserEdition


class EditionAdmin(admin.ModelAdmin):

    @admin.display(description="Votes Immor'Ale")
    def immor_ale_mean_votes_str(self, edition):
        return edition._get_ale_mean_votes_str("immor_ale")

    @admin.display(description="Votes Feod'Ale")
    def feod_ale_mean_votes_str(self, edition):
        return edition._get_ale_mean_votes_str("feod_ale")

    @admin.display(description="Votes Glaci'Ale")
    def glaci_ale_mean_votes_str(self, edition):
        return edition._get_ale_mean_votes_str("glaci_ale")

    @admin.display(description="Votes Spectr'Ale")
    def spectr_ale_mean_votes_str(self, edition):
        return edition._get_ale_mean_votes_str("spectr_ale")

    @admin.display(description="Votes Brut'Ale")
    def brut_ale_mean_votes_str(self, edition):
        return edition._get_ale_mean_votes_str("brut_ale")

    @admin.display(description="Votes Infern'Ale")
    def infern_ale_mean_votes_str(self, edition):
        return edition._get_ale_mean_votes_str("infern_ale")

    @admin.display(description="Votes Ancestr'Ale")
    def ancestr_ale_mean_votes_str(self, edition):
        return edition._get_ale_mean_votes_str("ancestr_ale")

    @admin.display(description="Votes Abyss'Ale")
    def abyss_ale_mean_votes_str(self, edition):
        return edition._get_ale_mean_votes_str("abyss_ale")

    @admin.display(description="Classement bières")
    def ale_votes_order(self, edition):
        votes = filter(
            lambda vote: vote[1] is not None,
            [
                ("Immor'Ale", edition._get_ale_mean_vote("immor_ale")),
                ("Feod'Ale", edition._get_ale_mean_vote("feod_ale")),
                ("Glaci'Ale", edition._get_ale_mean_vote("glaci_ale")),
                ("Spectr'Ale", edition._get_ale_mean_vote("spectr_ale")),
                ("Brut'Ale", edition._get_ale_mean_vote("brut_ale")),
                ("Infern'Ale", edition._get_ale_mean_vote("infern_ale")),
                ("Ancestr'Ale", edition._get_ale_mean_vote("ancestr_ale")),
                ("Abyss'Ale", edition._get_ale_mean_vote("abyss_ale")),
            ],
        )
        return ", ".join(
            map(
                lambda vote: f"{vote[0]}: {vote[1]}",
                sorted(votes, key=lambda vote: -vote[1]),
            )
        )

    list_display = (
        "year",
        "season",
        "start_date",
        "end_date",
        "is_current",
        "coming_count",
        "total_budget",
        "total_bank",
        "budget_due",
        "beer_budget",
        "closed_editions_total_not_spent",
        "total_budgeted",
        "not_budgeted",
        "total_expenses",
        "not_spent",
    )
    fields = (
        "year",
        "season",
        "is_current",
        "start_date",
        "end_date",
        "payable",
        "signal_group_id",
        "signal_transport_group_id",
        "lydia_link",
        "payment_iban",
        "payment_bic",
        "payment_owner_name",
        "photo_album_link",
        "coming_count",
        "total_budget",
        "total_bank",
        "budget_due",
        "beer_budget",
        "closed_editions_total_not_spent",
        "total_budgeted",
        "not_budgeted",
        "total_expenses",
        "not_spent",
        "immor_ale_mean_votes_str",
        "feod_ale_mean_votes_str",
        "glaci_ale_mean_votes_str",
        "spectr_ale_mean_votes_str",
        "brut_ale_mean_votes_str",
        "infern_ale_mean_votes_str",
        "ancestr_ale_mean_votes_str",
        "abyss_ale_mean_votes_str",
        "ale_votes_order",
    )
    readonly_fields = (
        "coming_count",
        "total_budget",
        "total_bank",
        "budget_due",
        "beer_budget",
        "closed_editions_total_not_spent",
        "total_budgeted",
        "not_budgeted",
        "total_expenses",
        "not_spent",
        "immor_ale_mean_votes_str",
        "feod_ale_mean_votes_str",
        "glaci_ale_mean_votes_str",
        "spectr_ale_mean_votes_str",
        "brut_ale_mean_votes_str",
        "infern_ale_mean_votes_str",
        "ancestr_ale_mean_votes_str",
        "abyss_ale_mean_votes_str",
        "ale_votes_order",
    )
    ordering = ("-year", "-season")
    inlines = [PresentInline]


admin.site.register(Edition, EditionAdmin)
