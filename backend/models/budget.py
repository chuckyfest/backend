from django.db.models import (
    Model,
    ForeignKey,
    CASCADE,
    DecimalField,
    CharField,
    BooleanField,
)
from django.contrib import admin


class Budget(Model):
    edition = ForeignKey("Edition", on_delete=CASCADE)
    name = CharField("Nom", max_length=100)
    target_percentage = DecimalField("Ratio visé", max_digits=7, decimal_places=2)
    is_beer_budget = BooleanField("Est le budget bière", default=False)

    def total_expenses(self):
        return sum(map(lambda expense: expense.amount, self.expense_set.all()))

    def amount(self):
        if not self.target_percentage:
            return 0
        return round(
            (self.edition.total_budget + self.edition.closed_editions_total_not_spent)
            * self.target_percentage
            / 100
        ) + (self.edition.beer_budget if self.is_beer_budget else 0)

    def difference_amount(self):
        if not self.amount():
            return 0
        return self.amount() - self.total_expenses()

    class Meta:
        app_label = "backend"

    def __str__(self):
        return f"Budget {self.name} de {str(self.edition)}"

    @classmethod
    def get_content_type(cls):
        from django.contrib.contenttypes.models import ContentType

        return ContentType.objects.get(app_label="backend", model="budget")


class BudgetAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "edition",
        "name",
        "amount",
        "total_expenses",
        "difference_amount",
        "target_percentage",
    )
    fields = (
        "edition",
        "name",
        "amount",
        "total_expenses",
        "difference_amount",
        "target_percentage",
        "is_beer_budget",
    )
    readonly_fields = ("total_expenses", "difference_amount", "amount")
    ordering = ("-edition", "name")
    list_filter = ("edition", "name")


admin.site.register(Budget, BudgetAdmin)
