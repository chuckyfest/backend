import datetime as dt

from django.db.models import (
    Model,
    ForeignKey,
    CASCADE,
    SET_NULL,
    IntegerField,
    DecimalField,
    BooleanField,
    DateTimeField,
    CharField,
    TextField,
)
from django.http.response import HttpResponse
from django.contrib import admin

from backend.enums import SleepingPlaces, TransportModes, FromCity


class UserEdition(Model):
    edition = ForeignKey("Edition", on_delete=CASCADE)
    user = ForeignKey("UserProfile", on_delete=CASCADE)

    is_coming = BooleanField("Vient à Chucky", null=True)
    probability = IntegerField("Probabilité", default=0)

    beer_amount_announced = DecimalField(
        "Montant bière annoncé", default=0, max_digits=7, decimal_places=2
    )
    paid_amount = DecimalField(
        "Montant payé", default=0, max_digits=7, decimal_places=2
    )

    start_datetime = DateTimeField("Date d'arrivée", null=True, blank=True)
    end_datetime = DateTimeField("Date de départ", null=True, blank=True)

    sleeping_place = CharField(
        "Lieu de dormage",
        max_length=SleepingPlaces.get_max_length(),
        blank=True,
        choices=SleepingPlaces.to_choices(),
    )
    ask_bedroom = CharField("Demande de chambre", max_length=1000, blank=True)
    want_to_canoe = BooleanField("Veut faire du canoë", default=False)
    accept_date_sharing = BooleanField("Accepte le partage de ses dates", default=False)
    invitation_list = TextField("Liste d'invité.e.s", blank=True)

    transport_mode = CharField(
        "Mode de transport",
        max_length=TransportModes.get_max_length(),
        blank=True,
        choices=TransportModes.to_choices(),
    )
    has_car = BooleanField("A une voiture", default=False)
    come_in_car_of = ForeignKey(
        "backend.UserProfile",
        verbose_name="Vient dans la voiture de",
        on_delete=SET_NULL,
        null=True,
        blank=True,
        related_name="brought_people",
    )
    from_city = CharField(
        "Ville de départ",
        max_length=FromCity.get_max_length(),
        blank=True,
        choices=FromCity.to_choices(),
    )
    transport_message = TextField(verbose_name="Annonce pour le transport", blank=True)

    class Meta:
        app_label = "backend"

    @property
    def contribution_amount(self):
        start = self.start_datetime
        end = self.end_datetime
        if not start or not end:
            return 0
        if start.hour < 8:
            start -= dt.timedelta(days=1)
        start = start.replace(hour=8, minute=0)
        if end.hour < 8:
            end -= dt.timedelta(days=1)
        end = end.replace(hour=8, minute=0)
        return (end - start).days * 20

    @property
    def due_amount(self):
        return max(
            0, self.contribution_amount + self.beer_amount_announced - self.paid_amount
        )

    @property
    def beer_amount(self):
        return max(0, self.paid_amount - self.contribution_amount)

    @property
    def transport_ok(self):
        return self.transport_mode == TransportModes.train.name or (
            self.transport_mode == TransportModes.car.name
            and (self.has_car or self.come_in_car_of is not None)
        )

    def __str__(self):
        return f"{str(self.user)} | {str(self.edition)}"

    @classmethod
    def get_content_type(cls):
        from django.contrib.contenttypes.models import ContentType

        return ContentType.objects.get(app_label="backend", model="useredition")


@admin.action(description="Récupérer mails")
def fetch_mail(modeladmin, request, queryset):
    url = request.META.get("HTTP_REFERER")
    emails = ";".join([ue.user.user.email for ue in queryset])
    return HttpResponse(
        f"""
        <div style="font-size: 3em; text-align: center">
            <input value="{emails}" style="width: 100%; font-size: 1em; " />
            <a href="{url}" style="display: block; margin: 100px; text-decoration: none; background: grey; padding: 30px; color: white">Retour</a>
            <script>document.getElementsByTagName('input')[0].select()</script>
        </div>
        """
    )


class UserEditionAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_display = (
        "user",
        "edition",
        "is_coming",
        "start_datetime",
        "end_datetime",
        "contribution_amount",
        "paid_amount",
        "beer_amount_announced",
        "due_amount",
        "beer_amount",
        "sleeping_place",
        "ask_bedroom",
        "want_to_canoe",
        "transport_message",
        "probability",
        "user",
    )
    fields = (
        "user",
        "edition",
        "probability",
        "contribution_amount",
        "paid_amount",
        "beer_amount_announced",
        "due_amount",
        "beer_amount",
        "is_coming",
        "start_datetime",
        "end_datetime",
        "sleeping_place",
        "ask_bedroom",
        "want_to_canoe",
        "accept_date_sharing",
        "transport_mode",
        "from_city",
        "has_car",
        "come_in_car_of",
        "transport_message",
        "invitation_list",
    )
    readonly_fields = ("contribution_amount", "due_amount", "beer_amount")
    list_filter = ("edition", "is_coming", "user")
    actions = [fetch_mail]
    search_fields = (
        "user__user__first_name",
        "user__user__last_name",
        "user__user__username",
    )


admin.site.register(UserEdition, UserEditionAdmin)
