import os
import sys
import socket

dir_path = os.path.dirname(os.path.realpath(__file__))
_location_l = os.path.normpath(os.path.realpath(__file__)).split(os.sep)

sys.path.append(dir_path)


def _check_exists(name):
    return "{file}.py".format(file=name) in os.listdir(dir_path)


def _resolve():
    if socket.gethostname() == "manu-NS5x-NS7xAU" and _location_l[2] == "manu":
        return "manu"
    elif (
        _location_l[2] == "jobup"
        and _location_l[3] == "Chucky"
        and _location_l[4] == "backend"
    ):
        return "prod"
    # elif (
    #     _location_l[2] == "jobup"
    #     and _location_l[3] == "Chucky"
    #     and _location_l[4] == "backend_dev"
    # ):
    #     return "dev"
    raise NotImplementedError(
        "Unknown host. Please configure _resolve.py to implement your host. "
        "Params you can use: FILE-->{loc}<-- HOST-->{host}<--".format(
            loc=_location_l, host=socket.gethostname()
        )
    )


def resolve_settings():
    config_name = _resolve()
    if not _check_exists(config_name):
        raise NameError(
            "Unknown configuration. You implemented a resolution ('{name}') which does not exists. "
            "Please create a config.".format(name=config_name)
        )
    return config_name


def get_settings(config_name=None):
    if config_name is None:
        config_name = resolve_settings()
    config = __import__(config_name, fromlist=("*",))
    default = __import__("default", fromlist=("*",))
    settings = {"DEBUG": True}
    for p in dir(default):
        if p[0] != "_" and p == p.upper():
            settings[p] = default.__dict__[p]
    for p in dir(config):
        if p[0] != "_" and p == p.upper():
            settings[p] = config.__dict__[p]

    return settings
