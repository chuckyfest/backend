"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import (
    LoginView as DjangoLoginView,
    LogoutView as DjangoLogoutView,
)
from backend.views import (
    cant_come_view,
    ChuckyFormView,
    IphoneChuckyFormView,
    ChuckWhoFormView,
    ChuckyView,
    EditionView,
    WhosComingView,
    token_login_view,
    mail_login_view,
    home_view,
)
from django.conf import settings


def view_500(request):
    raise Exception("Test !")


urlpatterns = [
    path("", home_view, name="home"),
    path("login-par-mail", mail_login_view, name="mail-login"),
    path("token/", token_login_view, name="token-login"),
    path("login/", DjangoLoginView.as_view(), name="login"),
    path("logout/", DjangoLogoutView.as_view(), name="logout"),
    path(settings.LOGIN_REDIRECT_URL[1:], ChuckyFormView.as_view(), name="chucky-form"),
    path(
        "jaiuniphonedemerde", IphoneChuckyFormView.as_view(), name="jaiuniphonedemerde"
    ),
    path("chucqui", ChuckWhoFormView.as_view(), name="chucky-who-form"),
    path("cant_come", cant_come_view, name="cant-come"),
    path("chucky", ChuckyView.as_view(), name="chucky"),
    path("qui-vient", WhosComingView.as_view(), name="whos-coming"),
    path("edition/<int:pk>", EditionView.as_view(), name="editions"),
    path("admin/", admin.site.urls),
    path("quiz/", include("quiz.urls") ),
    path("raise-500/", view_500, name="500"),
]
