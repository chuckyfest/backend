# Generated by Django 3.2 on 2022-03-23 10:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0018_auto_20220323_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='useredition',
            name='probability',
            field=models.IntegerField(default=0, verbose_name='Probabilité'),
        ),
    ]
