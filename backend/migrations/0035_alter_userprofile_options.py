# Generated by Django 3.2.3 on 2024-07-02 16:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0034_auto_20240605_1018'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userprofile',
            options={'ordering': ['user__first_name', 'user__last_name']},
        ),
    ]
