# Generated by Django 3.2 on 2021-11-11 08:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0009_edition_season'),
    ]

    operations = [
        migrations.AddField(
            model_name='edition',
            name='is_current',
            field=models.BooleanField(default=False, verbose_name='Édition en cours'),
        ),
    ]
