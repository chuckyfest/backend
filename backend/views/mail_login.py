from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, reverse
from django.conf import settings

from rest_framework.authtoken.models import Token

from backend.mailjet_connector import send_mail


def mail_login_view(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect(request.GET.get("next", reverse("chucky")))
        else:
            return render(request, "mail_login.html")
    else:
        email = request.POST.get("email", None)
        if email:
            try:
                user = User.objects.get(email=email.lower())
            except ObjectDoesNotExist:
                pass
            else:
                try:
                    token = user.auth_token
                except ObjectDoesNotExist:
                    token = Token.objects.create(user=user)
                if not settings.DEBUG:
                    res = send_mail(
                        "Ton mail de connexion",
                        settings.MJ_MAIL_LOGIN_ID,
                        [dict(Email=user.email, Name=user.first_name)],
                        dict(token=token.key),
                    )
                    if res.status_code == 200:
                        print("mail sent")
                else:
                    print(
                        f"Le lien de connexion : {request.build_absolute_uri(reverse('token-login'))}?token={token.key}"
                    )

                return render(request, "mail_login.html", context=dict(success=True))

        return render(request, "mail_login.html")
