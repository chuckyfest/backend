from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import UpdateView

from backend.forms import ChuckyForm, ChuckyWhoForm
from backend.models import UserProfile, Edition, UserEdition


class ChuckyFormView(LoginRequiredMixin, UpdateView):
    """
    Display the chucky form.
    """

    form_class = ChuckyForm
    success_url = "/chucky"

    def get_object(self, queryset=None):
        try:
            user_profile = UserProfile.objects.get(user=self.request.user)
        except ObjectDoesNotExist:
            user_profile = UserProfile.objects.create(user=self.request.user)

        return user_profile.get_current_user_edition()

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        user_edition = self.get_object()
        kw["instance"] = user_edition
        return kw

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        current_edition = Edition.objects.get(is_current=True)

        context["start_date"] = current_edition.start_date.strftime("%Y-%m-%d")
        context["end_date"] = current_edition.end_date.strftime("%Y-%m-%d")

        return context


class IphoneChuckyFormView(ChuckyFormView):
    template_name_suffix = "_form_styleless"


class ChuckWhoFormView(LoginRequiredMixin, UpdateView):
    """
    Display the chucky form.
    """

    form_class = ChuckyWhoForm
    success_url = "/chucky"

    def get_object(self, queryset=None):
        try:
            return UserProfile.objects.get(user=self.request.user)
        except ObjectDoesNotExist:
            return UserProfile.objects.create(user=self.request.user)

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        user_profile = self.get_object()
        kw["instance"] = user_profile
        return kw
