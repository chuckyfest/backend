from django.shortcuts import HttpResponse


def cant_come_view(request):
    user_edition = request.user.user_profile.get_current_user_edition()
    user_edition.is_coming = False
    user_edition.save()
    return HttpResponse("tristesse.")
