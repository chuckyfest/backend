from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.conf import settings
from django.contrib.auth import login

from rest_framework.authtoken.models import Token


def get_token_from_key(token_key: str):
    try:
        return Token.objects.get(key=token_key)
    except ObjectDoesNotExist:
        return None


def get_token_from_request(request):
    return request.GET.get("token", None)


def token_login_view(request):
    token_key = get_token_from_request(request)
    if token_key:
        token = get_token_from_key(token_key)
        if token:
            login(request, token.user)
            next_url: str = request.GET.get("next", settings.LOGIN_REDIRECT_URL)
            return redirect(next_url)
    # in any other cases, redirects to login
    return redirect(settings.LOGIN_URL)
