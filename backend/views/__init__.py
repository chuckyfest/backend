from .cant_come import cant_come_view
from .chucky import ChuckyView
from .chucky_form import ChuckyFormView, IphoneChuckyFormView, ChuckWhoFormView
from .edition import EditionView
from .whos_coming import WhosComingView
from .token_login import token_login_view
from .home import home_view
from .mail_login import mail_login_view
