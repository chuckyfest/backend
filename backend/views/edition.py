from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView

from backend.models import Edition


class EditionView(LoginRequiredMixin, DetailView):
    """
    Display the previous editions.
    """

    queryset = Edition.objects.all()
