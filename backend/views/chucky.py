import os

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.views.generic import DetailView

from backend.models import UserProfile, Edition, UserEdition
from quiz.models import Quiz


class ChuckyView(LoginRequiredMixin, DetailView):
    """
    Display the chucky infos.
    """

    def get_object(self, queryset=None):
        current_edition = Edition.objects.get(is_current=True)
        user_profile = UserProfile.objects.get(user=self.request.user)

        try:
            return UserEdition.objects.get(user=user_profile, edition=current_edition)
        except ObjectDoesNotExist:
            return UserEdition.objects.create(
                user=user_profile, edition=current_edition
            )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        current_edition = Edition.objects.get(is_current=True)
        context["current_edition"] = current_edition

        previous_edition = (
            Edition.objects.filter(is_current=False).order_by("-year").first()
        )
        context["previous_edition"] = previous_edition

        signal_link = f"https://signal.group/#{current_edition.signal_group_id}"
        context["signal_link"] = signal_link

        signal_transport_link = (
            f"https://signal.group/#{current_edition.signal_transport_group_id}"
        )
        context["signal_transport_link"] = signal_transport_link

        invitation_file_path = (
            f"{settings.STATIC_ROOT}"
            f"/event_invitations/"
            f"{current_edition.get_event_invitation_file_name()}"
        )
        context["event_invitation_file_exists"] = os.path.exists(invitation_file_path)

        context["previous_editions"] = (
            self.request.user.user_profile.chucky_editions.all()
        )

        context["published_quiz"] = Quiz.objects.filter(is_published=True)

        paid_distribution = [0, 0, 0, 0, 0, 0, 0]
        announced_distribution = [0, 0, 0, 0, 0, 0, 0]

        for ue in current_edition.coming_persons:
            if ue.beer_amount_announced:
                announced_distribution[int(ue.beer_amount_announced / 10)] += 1
            else:
                paid_distribution[int(ue.beer_amount / 10)] += 1

        unknown_count = current_edition.useredition_set.filter(
            is_coming__isnull=True
        ).count()

        context["paid_distribution"] = [0] + paid_distribution
        context["announced_distribution"] = [0] + announced_distribution
        context["unknown_distribution"] = [unknown_count, 0, 0, 0, 0, 0, 0, 0]

        return context
