from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from backend.models import UserEdition


class WhosComingView(LoginRequiredMixin, ListView):
    """
    Display the participants to chucky.
    """

    queryset = UserEdition.objects.filter(
        is_coming=True,
        edition__is_current=True,
        start_datetime__isnull=False,
        end_datetime__isnull=False,
    )
