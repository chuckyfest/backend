import datetime as dt
from django.shortcuts import render
from django.contrib.auth import login

from backend.views.token_login import get_token_from_key, get_token_from_request
from backend.models import Edition


def home_view(request):
    token_key = get_token_from_request(request)
    if token_key:
        token = get_token_from_key(token_key)
        if token:
            login(request, token.user)

    current_edition = Edition.objects.get(is_current=True)

    dates = [
        current_edition.start_date + dt.timedelta(days=i)
        for i in range((current_edition.end_date - current_edition.start_date).days + 1)
    ]

    dates_by_month = {}
    for d in dates:
        month = d.strftime("%b")
        if month not in dates_by_month:
            dates_by_month[month] = []
        dates_by_month[month].append(d.day)

    years = set(map(lambda d: d.year, dates))

    return render(
        request,
        "index.html",
        dict(
            current_edition=current_edition,
            dates_by_month=dates_by_month,
            years=years,
            start_date=current_edition.start_date,
            end_date=current_edition.end_date,
        ),
    )
