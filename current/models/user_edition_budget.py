from django.contrib import admin

from backend.models import UserEdition
from backend.models.user_edition import UserEditionAdmin


class CurrentUserEditionBudget(UserEdition):
    class Meta:
        app_label = "current"
        proxy = True
        verbose_name = "Personne - Budget"
        verbose_name_plural = "Personne - Budget"


class CurrentUserEditionBudgetAdmin(UserEditionAdmin):
    ordering = ["id"]
    list_display = (
        "user",
        "is_coming",
        "contribution_amount",
        "paid_amount",
        "beer_amount_announced",
        "due_amount",
        "beer_amount",
        "user",
    )
    list_filter = ("is_coming", "user")

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .filter(
                edition__is_current=True,
                start_datetime__isnull=False,
                end_datetime__isnull=False,
            )
        )


admin.site.register(CurrentUserEditionBudget, CurrentUserEditionBudgetAdmin)
