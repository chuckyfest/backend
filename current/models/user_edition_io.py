from django.contrib import admin
from django.contrib.admin import SimpleListFilter

from backend.models import UserEdition
from backend.models.user_edition import UserEditionAdmin


class CurrentUserEditionIO(UserEdition):
    class Meta:
        app_label = "current"
        proxy = True
        verbose_name = "Personne - Arrivées/Départs"
        verbose_name_plural = "Personne - Arrivées/Départs"


class TransportOKFilter(SimpleListFilter):
    title = "Transport OK"
    parameter_name = "transport_ok"

    def lookups(self, request, model_admin):
        return [
            (True, "Oui"),
            (False, "Non"),
        ]

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset

        not_ok = []
        for el in queryset:
            if not el.transport_ok:
                not_ok.append(el.id)

        if self.value() == "True":
            return queryset.exclude(id__in=not_ok)
        else:
            return queryset.filter(id__in=not_ok)


class CurrentUserEditionIOAdmin(UserEditionAdmin):
    @admin.display(description="Transport OK", boolean=True)
    def transport_ok(self, current_user_edition):
        return current_user_edition.transport_ok

    ordering = ["id"]
    list_display = (
        "user",
        "is_coming",
        "start_datetime",
        "end_datetime",
        "transport_ok",
        "transport_mode",
        "has_car",
        "come_in_car_of",
        "from_city",
        "transport_message",
        "probability",
        "user",
    )
    list_filter = (
        "is_coming",
        "transport_mode",
        "has_car",
        "from_city",
        TransportOKFilter,
        "probability",
        "user",
    )

    def get_queryset(self, request):
        return super().get_queryset(request).filter(edition__is_current=True)


admin.site.register(CurrentUserEditionIO, CurrentUserEditionIOAdmin)
