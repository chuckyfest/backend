from django.db.models import (
    Model,
    ForeignKey,
    CASCADE,
    CharField,
    BooleanField,
)
from django.contrib import admin
from django.shortcuts import reverse

from .question import Question


class Choice(Model):
    question = ForeignKey(Question, on_delete=CASCADE)
    name = CharField("Nom", max_length=200)
    is_valid_answer = BooleanField("Est la bonne réponse", default=False)

    def __str__(self):
        return f"{self.question} => {self.name}"

    @property
    def answer_url(self):
        return reverse(
            "quiz:quiz_answer", kwargs=dict(pk=self.question.quiz.id, choice_id=self.id)
        )


class AnswerInline(admin.TabularInline):
    from .answer import Answer

    model = Answer
    fk_name = "choice"


class ChoiceAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_display = ("question", "name", "is_valid_answer")
    fields = ("question", "name", "is_valid_answer")
    inlines = [AnswerInline]

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj=None, change=False, **kwargs)
        form.base_fields["question"].queryset = Question.objects.filter(
            quiz__owner=request.user.user_profile
        )
        return form

    def get_queryset(self, request):
        if request.user.is_superuser:
            return super().get_queryset(request)
        return (
            super()
            .get_queryset(request)
            .filter(question__quiz__owner=request.user.user_profile)
        )

    class Meta:
        app_label = "quiz"
        verbose_name = "Choix"
        verbose_name_plural = "Choix"


admin.site.register(Choice, ChoiceAdmin)
