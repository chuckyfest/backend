from django.db.models import (
    Model,
    ForeignKey,
    CASCADE,
    CharField,
    BooleanField,
)
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.contrib import admin

from backend.models import Edition, UserProfile


class Quiz(Model):
    name = CharField("Nom", max_length=200)
    edition = ForeignKey(Edition, on_delete=CASCADE)
    owner = ForeignKey(UserProfile, on_delete=CASCADE)
    is_published = BooleanField("Est publié", default=False)
    is_open = BooleanField("Est démarré", default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "quiz"
        verbose_name_plural = "quiz"

    @property
    def is_finished(self):
        return self.question_set.filter(stop_datetime__isnull=True).count() == 0


class QuestionInline(admin.TabularInline):
    from .question import Question

    model = Question
    fields = ("question",)
    fk_name = "quiz"
    extra = 0
    show_change_link = True
    can_delete = False


class ParticipationInline(admin.TabularInline):
    from .participation import Participation

    def score(self, participation: Participation):
        return participation.score

    score.short_description = "score"

    def rank(self, participation: Participation):
        return participation.rank

    rank.short_description = "rang"

    model = Participation
    fields = ("profile", "score", "rank")
    readonly_fields = ("score", "rank", "profile")
    can_delete = False
    extra = 0


class QuizAdmin(admin.ModelAdmin):
    @admin.display(description="Est terminé", boolean=True)
    def is_finished(self, quiz: Quiz):
        return quiz.is_finished

    ordering = ["id"]
    list_display = (
        "name",
        "edition",
        "owner",
        "is_published",
        "is_open",
        "link_to_answers",
    )
    fields = (
        "name",
        "edition",
        "owner",
        "is_published",
        "is_open",
        "is_finished",
        "link_to_answers",
    )
    readonly_fields = ("owner", "edition", "link_to_answers", "is_finished")
    inlines = [QuestionInline, ParticipationInline]

    def link_to_answers(self, obj):
        changelist_url = reverse("admin:quiz_answer_changelist")
        link = f"{changelist_url}?choice__question__quiz__id__exact={obj.pk}"
        html = f'<a href="{link}" style="border: 1px solid currentColor; padding: 1px 4px; border-radius: 2px;">→ VOIR LES RÉPONSES</a>'
        return mark_safe(html)

    link_to_answers.short_description = "réponses"

    def get_queryset(self, request):
        if request.user.is_superuser:
            return super().get_queryset(request)
        return super().get_queryset(request).filter(owner=request.user.user_profile)

    def save_model(self, request, obj, form, change):
        obj.owner = request.user.user_profile
        obj.edition = Edition.objects.get(is_current=True)
        super().save_model(request, obj, form, change)

    class Meta:
        app_label = "quiz"
        verbose_name = "Quiz"
        verbose_name_plural = "Quiz"


admin.site.register(Quiz, QuizAdmin)
