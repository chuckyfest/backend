from django.db.models import (
    Model,
    ForeignKey,
    CASCADE,
    DateTimeField,
    IntegerField,
    BooleanField,
)
from django.contrib import admin

from .choice import Choice
from .participation import Participation


class Answer(Model):
    participation = ForeignKey(
        Participation, verbose_name="Participation", on_delete=CASCADE
    )
    choice = ForeignKey(Choice, verbose_name="Choix", on_delete=CASCADE)
    is_correct = BooleanField("Est correct", default=False)
    timestamp = DateTimeField(verbose_name="Timing", auto_now_add=True)
    rank = IntegerField(verbose_name="Rang", null=True, blank=True)
    score = IntegerField(verbose_name="Score", default=0)

    def __str__(self):
        return f"{self.choice.question} => { self.participation.profile} a répondu {self.choice.name}"

    @property
    def time_diff(self):
        if not self.is_correct:
            return None
        valid_choices = self.choice.question.choice_set.filter(is_valid_answer=True)
        first_valid_answer = Answer.objects.filter(choice__in=valid_choices).order_by(
            "timestamp"
        )[0]
        return int((self.timestamp - first_valid_answer.timestamp).total_seconds())

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        self.is_correct = (
            self.choice.question.choice_set.filter(
                id=self.choice.id, is_valid_answer=True
            ).count()
            == 1
        )

        if not self.is_correct:
            self.score = -1
        else:
            valid_choices = self.choice.question.choice_set.filter(is_valid_answer=True)
            valid_answers = Answer.objects.filter(choice__in=valid_choices).order_by(
                "timestamp"
            )
            if not self.id or self.id not in list(
                valid_answers.values_list("id", flat=True)
            ):
                self.rank = len(valid_answers) + 1
            else:
                self.rank = (
                    list(valid_answers.values_list("id", flat=True)).index(self.id) + 1
                )

            if self.rank == 1:
                self.score = 5
            elif self.rank == 2:
                self.score = 3
            elif self.rank == 3:
                self.score = 2
            else:
                self.score = 1

        super().save(force_insert, force_update, using, update_fields)


class AnswerAdmin(admin.ModelAdmin):
    @admin.display(description="Partipant⋅e")
    def participation_profile(self, answer: Answer):
        return answer.participation.profile

    @admin.display(description="Quiz")
    def quiz(self, answer: Answer):
        return answer.choice.question.quiz

    @admin.display(description="Question")
    def question(self, answer: Answer):
        return answer.choice.question.question

    @admin.display(description="Choix")
    def choice_name(self, answer: Answer):
        return answer.choice.name

    @admin.display(description="Est correct", boolean=True)
    def is_correct(self, answer: Answer):
        return answer.is_correct

    @admin.display(description="Rang")
    def rank(self, answer: Answer):
        return answer.rank

    @admin.display(description="Diff timing")
    def time_diff(self, answer: Answer):
        return answer.time_diff

    ordering = ["timestamp"]
    list_display = (
        "participation_profile",
        "choice_name",
        "is_correct",
        "rank",
        "score",
        "time_diff",
        "quiz",
        "question",
        "timestamp",
    )
    fields = (
        "quiz",
        "question",
        "participation",
        "choice",
        "timestamp",
        "is_correct",
        "rank",
        "score",
        "time_diff",
    )
    readonly_fields = (
        "quiz",
        "question",
        "timestamp",
        "is_correct",
        "rank",
        "score",
        "time_diff",
    )

    list_filter = (
        "choice__question__quiz",
        "is_correct",
        "rank",
        "score",
        "participation__profile",
    )

    def lookup_allowed(self, lookup, value):
        if lookup in (
            "choice__question__id__exact",
            "choice__question__quiz__id__exact",
        ):
            return True
        return super().lookup_allowed(lookup, value)

    def get_queryset(self, request):
        if request.user.is_superuser:
            return super().get_queryset(request)
        return (
            super()
            .get_queryset(request)
            .filter(choice__question__quiz__owner=request.user.user_profile)
        )

    class Meta:
        app_label = "quiz"
        verbose_name = "Réponse"


admin.site.register(Answer, AnswerAdmin)
