from django.db.models import Model, ForeignKey, CASCADE
from django.contrib import admin

from backend.models import UserProfile
from .quiz import Quiz


class Participation(Model):
    profile = ForeignKey(UserProfile, on_delete=CASCADE)
    quiz = ForeignKey(Quiz, on_delete=CASCADE)

    @property
    def score(self):
        score = 0
        for answer in self.answer_set.all():
            score += answer.score
        return score

    @property
    def rank(self):
        participations = self.quiz.participation_set.all()
        scores = sorted([(p, p.score) for p in participations], key=lambda pp: -pp[1])
        for i, (p, s) in enumerate(scores):
            if p.id == self.id:
                return i + 1

    def __str__(self):
        return f"{self.profile} participe au quiz {self.quiz}"

    class Meta:
        unique_together = (("profile", "quiz"),)


class ParticipationAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_display = ("profile", "quiz")
    fields = ("profile", "quiz")
    list_filter = ("profile", "quiz")

    class Meta:
        app_label = "quiz"
        verbose_name = "Participation"
        verbose_name_plural = "Participation"

    def get_queryset(self, request):
        if request.user.is_superuser:
            return super().get_queryset(request)
        return (
            super().get_queryset(request).filter(quiz__owner=request.user.user_profile)
        )


admin.site.register(Participation, ParticipationAdmin)
