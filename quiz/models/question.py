from django.contrib import messages
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.http import HttpResponseRedirect
from django.db.models import (
    Model,
    ForeignKey,
    CASCADE,
    CharField,
    DateTimeField,
)
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.contrib import admin

from .quiz import Quiz


class Question(Model):
    quiz = ForeignKey(Quiz, on_delete=CASCADE)
    question = CharField("Question", max_length=400)
    start_datetime = DateTimeField("début", null=True)
    stop_datetime = DateTimeField("fin", null=True)

    def __str__(self):
        return f"{self.quiz.name} : {self.question}"


class ChoiceInline(admin.TabularInline):
    from .choice import Choice

    model = Choice
    fk_name = "question"


class AnswerInline(admin.TabularInline):
    from .answer import Answer

    model = Answer

    def get_queryset(self, request):
        return super().get_queryset(request).filter(choice__question=self.parent_model)


@admin.action(description="Dupliquer")
def duplicate_question(modeladmin, request, queryset):
    from .choice import Choice

    for question in queryset:
        new_question = Question.objects.create(
            quiz=question.quiz, question=question.question
        )
        for choice in question.choice_set.all():
            Choice.objects.create(question=new_question, name=choice.name)


class QuestionAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_display = (
        "quiz",
        "question",
        "start_datetime",
        "stop_datetime",
        "link_to_answers",
    )
    fields = (
        "quiz",
        "question",
        "start_datetime",
        "stop_datetime",
        "chrono",
        "link_to_answers",
    )
    inlines = [ChoiceInline]
    readonly_fields = (
        "chrono",
        "link_to_answers",
        "start_datetime",
        "stop_datetime",
    )
    list_filter = ("quiz",)
    actions = [duplicate_question]

    def chrono(self, obj: Question):
        if obj.start_datetime and not obj.stop_datetime:
            html = (
                f"<button "
                f'class="button" style="background-color: var(--delete-button-bg)"'
                f'type="submit"'
                f'name="stop_chrono"'
                f'value="true">'
                f"ARRÊTER LE CHRONO"
                f"</button>"
            )
        elif not obj.start_datetime:
            html = (
                f"<button "
                f'class="button" style="background-color: var(--message-success-bg)"'
                f'type="submit"'
                f'name="start_chrono"'
                f'value="true">'
                f"DÉMARRER LE CHRONO"
                f"</button>"
            )
        else:
            html = (
                f"<button "
                f'class="button" style="background-color: var(--delete-button-bg)"'
                f'type="submit"'
                f'name="reset_chrono"'
                f'value="true">'
                f"RESET LE CHRONO"
                f"</button>"
            )
        return mark_safe(html)

    chrono.short_description = "Chrono"

    def link_to_answers(self, obj):
        changelist_url = reverse("admin:quiz_answer_changelist")
        link = f"{changelist_url}?choice__question__id__exact={obj.pk}"
        html = f'<a href="{link}" style="border: 1px solid currentColor; padding: 1px 4px; border-radius: 2px;">→ VOIR LES RÉPONSES</a>'
        return mark_safe(html)

    link_to_answers.short_description = "réponses"

    def response_change(self, request, obj: Question):
        start_chrono = request.POST.get("start_chrono", False)
        stop_chrono = request.POST.get("stop_chrono", False)
        reset_chrono = request.POST.get("reset_chrono", False)
        if start_chrono or stop_chrono or reset_chrono:
            if start_chrono:
                if (
                    obj.quiz.question_set.filter(
                        start_datetime__isnull=False,
                        stop_datetime__isnull=True,
                    ).count()
                    != 0
                ):
                    self.message_user(
                        request, "Une question est déjà en cours", messages.ERROR
                    )
                else:
                    obj.start_datetime = timezone.now()
                    self.message_user(
                        request, "Le chrono a bien été lancé", messages.SUCCESS
                    )
            elif stop_chrono:
                obj.stop_datetime = timezone.now()
                self.message_user(
                    request, "Le chrono a bien été arrêté", messages.SUCCESS
                )
            elif reset_chrono:
                obj.start_datetime = None
                obj.stop_datetime = None
                self.message_user(
                    request, "Le chrono a bien été reset", messages.SUCCESS
                )
            obj.save()

            preserved_filters = self.get_preserved_filters(request)
            redirect_url = add_preserved_filters(
                {"preserved_filters": preserved_filters, "opts": self.model._meta},
                request.path,
            )
            return HttpResponseRedirect(redirect_url)
        return super().response_change(request, obj)

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj=None, change=False, **kwargs)
        form.base_fields["quiz"].queryset = Quiz.objects.filter(
            owner=request.user.user_profile
        )
        return form

    def get_queryset(self, request):
        if request.user.is_superuser:
            return super().get_queryset(request)
        return (
            super().get_queryset(request).filter(quiz__owner=request.user.user_profile)
        )

    class Meta:
        app_label = "quiz"
        verbose_name = "Question"


admin.site.register(Question, QuestionAdmin)
