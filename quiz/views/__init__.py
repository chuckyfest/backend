from .quiz import QuizView
from .register import RegisterQuizView
from .answer import AnswerQuizView
