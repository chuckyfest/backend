from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django.shortcuts import redirect, reverse
from django.contrib import messages

from quiz.models import Quiz, Participation, Answer, Choice


class AnswerQuizView(LoginRequiredMixin, DetailView):
    """
    Answer to quiz
    """

    queryset = Quiz.objects.all()

    def get(self, request, choice_id, *args, **kwargs):
        quiz = self.get_object()
        try:
            participation = Participation.objects.get(
                quiz=quiz, profile=self.request.user.user_profile
            )
        except ObjectDoesNotExist:
            messages.add_message(request, messages.ERROR, "Vous n'êtes pas inscrit")
            return redirect(reverse("quiz:quiz", kwargs=dict(pk=quiz.id)))

        choice = Choice.objects.get(id=choice_id)

        if Answer.objects.filter(
            participation=participation,
            choice__in=choice.question.choice_set.all(),
            is_correct=True,
        ).count():
            messages.add_message(
                request, messages.SUCCESS, "Vous avez déjà trouvé la bonne réponse"
            )
            return redirect(reverse("quiz:quiz", kwargs=dict(pk=quiz.id)))

        answer = Answer.objects.create(choice_id=choice_id, participation=participation)

        if answer.is_correct:
            messages.add_message(
                request,
                messages.SUCCESS,
                f'"{answer.choice.name}" est une bonne réponse',
            )
        else:
            messages.add_message(
                request,
                messages.ERROR,
                f'"{answer.choice.name}" est une mauvaise réponse',
            )

        return redirect(reverse("quiz:quiz", kwargs=dict(pk=quiz.id)))
