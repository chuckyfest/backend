from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django.shortcuts import redirect, reverse

from quiz.models import Quiz, Participation


class RegisterQuizView(LoginRequiredMixin, DetailView):
    """
    Register to quiz
    """

    queryset = Quiz.objects.all()

    def get(self, request, *args, **kwargs):
        quiz = self.get_object()
        try:
            Participation.objects.get(quiz=quiz, profile=self.request.user.user_profile)
        except ObjectDoesNotExist:
            Participation.objects.create(
                quiz=quiz, profile=self.request.user.user_profile
            )
        return redirect(reverse("quiz:quiz", kwargs=dict(pk=quiz.id)))
