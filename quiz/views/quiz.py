from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django.shortcuts import   reverse

from quiz.models import Quiz, Question, Answer


class QuizView(LoginRequiredMixin, DetailView):
    """
    Display the quiz infos.
    """

    queryset = Quiz.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        quiz: Quiz = self.get_object()

        participations = quiz.participation_set.filter(
            profile=self.request.user.user_profile
        )
        current_participation = participations[0] if participations else None
        context["current_participation"] = current_participation

        current_questions = Question.objects.filter(
            quiz=quiz, start_datetime__isnull=False, stop_datetime__isnull=True
        ).order_by("-start_datetime")

        current_question = current_questions[0] if current_questions else None
        context["current_question"] = current_question

        if current_question:
            answers = Answer.objects.filter(
                choice__question=current_question, participation=current_participation
            )
            correct_answers = [a for a in answers if a.is_correct]
            correct_answer = correct_answers[0] if correct_answers else None
            context["correct_answer"] = correct_answer
            context["current_score"] = sum([a.score for a in answers])

        context["register_link"] = reverse(
            "quiz:quiz_register", kwargs=dict(pk=quiz.id)
        )

        return context
